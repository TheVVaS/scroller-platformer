js = {

    lastCallCounter: 0,
    lastCall: null,
    fps: null,

    canvas: null,
    ctx: null,

    init: function () {

        // get canvas and it's context
        js.canvas = document.getElementById('play-screen');
        js.ctx = js.canvas.getContext('2d');

        js.events();

        js.loop();
    },

    events: function() {
        window.addEventListener('keydown', function(event) {
            switch (event.key) {
                case 'ArrowLeft':  js.playerMoveLeft  = true; break;
                case 'ArrowRight': js.playerMoveRight = true; break;
                case 'ArrowUp':    js.playerJump      = true; break;
            }
        });
        window.addEventListener('keyup', function(event) {
            switch (event.key) {
                case 'ArrowLeft':  js.playerMoveLeft  = false; break;
                case 'ArrowRight': js.playerMoveRight = false; break;
                case 'ArrowUp':    js.playerJump      = false; break;
            }
        });
        window.addEventListener('keydown', function (event) {
            if (event.key === 'ArrowUp') js.playerActionJump();
        });
    },

    loop: function () {

        requestAnimationFrame( js.loop );

        js.showFps();




        js.updatePlayerMoveSpeed();
        js.updatePlayerFallSpeed();

        js.checkCollision();

        js.updatePlayerPosition();




        js.clearCanvas();

        js.drawPlayer();
        js.drawTestWall();
    },

    showFps: function () {

        if (js.fps == null) {
            js.lastCall = performance.now();
            js.fps = 0;
        }

        if (js.lastCallCounter>30) {

            let delta = ((performance.now() - js.lastCall)/1000)/30;
            js.lastCall = performance.now();
            js.fps = 1/delta;

            document.getElementById('test').innerText = '!! ' + Math.floor(js.fps*10)/10 + ' !!';
            js.lastCallCounter = 0;
        }

        js.lastCallCounter++;
    },

    //// WORLD CONSTANTS

    friction: 0.95,
    gravityAcceleration: 0.5,

    //// PLAYER MOVEMENT

    playerPosition: [400,400],

    playerMoveLeft: false,
    playerMoveRight: false,
    playerJump: false,

    playerVerticalSpeed: 0,
    playerHorizontalSpeed: 0,

    playerMaxVerticalSpeed: 10,
    playerMaxHorizontalSpeed: 4,

    playerMovementSpeed: 0.5,
    playerJumpPower: 10,
    playerInAir: true,

    updatePlayerMoveSpeed: function () {

        if (js.playerMoveLeft) {
            js.playerHorizontalSpeed -= js.playerMovementSpeed;
            if (js.playerHorizontalSpeed < -js.playerMaxHorizontalSpeed) js.playerHorizontalSpeed = -js.playerMaxHorizontalSpeed;
        } else if (js.playerMoveRight) {
            js.playerHorizontalSpeed += js.playerMovementSpeed;
            if (js.playerHorizontalSpeed > js.playerMaxHorizontalSpeed) js.playerHorizontalSpeed = js.playerMaxHorizontalSpeed;
        } else if (Math.abs(js.playerHorizontalSpeed) > 0.2) {
            js.playerHorizontalSpeed *= js.friction;
        } else {
            js.playerHorizontalSpeed = 0;
        }
    },

    updatePlayerFallSpeed: function () {


        if (js.playerInAir)
        js.playerVerticalSpeed += js.gravityAcceleration;

        if (js.playerVerticalSpeed > js.playerMaxVerticalSpeed) js.playerVerticalSpeed = js.playerMaxVerticalSpeed;
        if (js.playerVerticalSpeed < -js.playerMaxVerticalSpeed) js.playerVerticalSpeed = -js.playerMaxVerticalSpeed;
    },

    updatePlayerPosition: function() {
        js.playerPosition = [
            js.playerPosition[0] += js.playerHorizontalSpeed,
            js.playerPosition[1] += js.playerVerticalSpeed,
        ];
    },

    //// PLAYER ACTIONS

    playerActionJump: function () {
        js.playerVerticalSpeed = -10;
        js.playerInAir = true;
    },

    //// PLAYER COLLISION

    checkCollision: function () {

        js.wallsPositions.forEach(function (wallPosition) {

            let playerNextPos = [
                js.playerPosition[0] += js.playerHorizontalSpeed,
                js.playerPosition[1] += js.playerVerticalSpeed,
            ];

            if (!(
                wallPosition[0] > playerNextPos[0]+10 ||
                wallPosition[0]+10 < playerNextPos[0] ||
                wallPosition[1] > playerNextPos[1]+10 ||
                wallPosition[1]+10 < playerNextPos[1]
            )) {
                js.playerVerticalSpeed = 0;
                js.playerInAir = false;
            }

        });
    },

    //// WALLS

    wallsPositions: [
        [400,750]
    ],

    //// DRAWING

    drawPlayer: function () {

        js.ctx.fillStyle = '#FF0000';
        js.ctx.fillRect(js.playerPosition[0], js.playerPosition[1], 10, 10);
    },

    drawTestWall: function () {

        js.ctx.fillStyle = '#000000';
        js.ctx.fillRect(js.wallsPositions[0][0], js.wallsPositions[0][1], 10, 10);

    },

    clearCanvas: function () {
        js.ctx.clearRect(0, 0, js.canvas.width, js.canvas.height);
    },

};

js.init();